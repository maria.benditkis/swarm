"""
29.9.19
Swarm version 4. Same as Version 2 but with acceleration, numpy and ranges.
"""
from PyQt4 import QtGui
from PyQt4.QtCore import QTimer, Qt
from numpy import random, array
import math
import sys


class Dot(object):
    def __init__(self, location, velocity, acceleration, max_speed):
        self.location = location
        self.velocity = velocity
        self.acceleration = acceleration
        self.max_speed = max_speed

    def update(self, time, acceleration):
        self.location = self.location + (self.velocity*time) + (acceleration*(time**2))
        velocity = self.velocity + (acceleration*time)
        if self.max_speed and magnitude(velocity) > self.max_speed:
            self.velocity = unit(velocity)*self.max_speed
        else:
            self.velocity = velocity

    def following_acceleration(self, dot):
        return unit(dot.location - self.location)*self.acceleration


class Swarm(object):
    def __init__(self, swarm_size, borders, acc_mag_range, max_speed_range):
        self.swarm_size = swarm_size
        self.borders = borders
        self.dots = None
        self.following = None
        self.acc_mag_range = acc_mag_range
        self.max_speed_range = max_speed_range
        self.zero_acceleration = False

    def create_dots(self):
        dots = []
        for i in xrange(self.swarm_size):
            dots += [Dot(array([random.randint(self.borders[0]), random.randint(self.borders[1])]),
                         array([0, 0]),
                         random.randint(*self.acc_mag_range),
                         random.randint(*self.max_speed_range))]
        self.dots = dots

    def create_following(self):
        following = []
        for i in xrange(self.swarm_size):
            follow = random.randint(self.swarm_size)
            while follow == i:
                follow = random.randint(self.swarm_size)
            following += [follow]
        self.following = following

    def update_dots(self, time):
        for i in xrange(self.swarm_size):
            dot = self.dots[i]
            following = self.dots[self.following[i]]
            same_location = dot.location == following.location
            if self.zero_acceleration or (same_location[0] and same_location[1]):
                dot.update(time, array([0, 0]))
            else:
                dot.update(time, dot.following_acceleration(following))
        # print self.dots[0].velocity.magnitude()


class Window(QtGui.QMainWindow):
    def __init__(self, width, height, swarm, update_interval):
        super(Window, self).__init__()
        self.resize(width, height)
        self.setStyleSheet("background-color: black;")
        self.swarm = swarm
        self.update_interval = update_interval
        self.update_timer = QTimer()
        self.update_timer.timeout.connect(self.update)
        self.zero_acceleration = False

    def start_swarm(self):
        self.swarm.create_dots()
        self.swarm.create_following()
        self.update_timer.start(1000*self.update_interval)

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        pen = QtGui.QPen()
        pen.setWidth(4)
        pen.setCapStyle(Qt.RoundCap)
        pen.setColor(Qt.white)
        painter.setPen(pen)

        self.swarm.update_dots(self.update_interval)
        for dot in self.swarm.dots:
            painter.drawPoint(*dot.location)

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Space:
            print 'zero_acceleration ' + str(self.zero_acceleration)
            if self.zero_acceleration:
                self.zero_acceleration = False
                self.swarm.zero_acceleration = False
                self.swarm.create_following()
            else:
                self.zero_acceleration = True
                self.swarm.zero_acceleration = True


def magnitude(vector):
    return math.sqrt(vector[0] ** 2 + vector[1] ** 2)


def unit(vector):
    return vector/magnitude(vector)


my_swarm_size = 1000
window_width = 1300
window_height = 650
my_acc_mag_range = (200, 400)
my_update_interval = 1.0/24.0
my_max_speed_range = (50, 150)
my_num_following = 10
my_swarm = Swarm(my_swarm_size, (window_width, window_height), my_acc_mag_range, my_max_speed_range)

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    window = Window(window_width, window_height, my_swarm, my_update_interval)
    window.show()
    window.start_swarm()
    sys.exit(app.exec_())
