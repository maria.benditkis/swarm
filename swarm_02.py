"""
29.9.19
Swarm version 2. Moving with constant force and max speed.
"""
from PyQt4 import QtGui
from PyQt4.QtCore import QTimer, Qt
import numpy.random as random
import math
import sys


class Vector(object):

    def __init__(self, x, y):
        self.x = float(x)
        self.y = float(y)

    def __add__(self, other):
        if isinstance(other, Vector):
            answer = Vector(self.x + other.x, self.y + other.y)
            # print '__add__ %s + %s = %s' % (self, other, answer)
            return answer
        # else:
        #     print '__add__ %s: %s' % (str(type(other)), str(other))

    def __sub__(self, other):
        if isinstance(other, Vector):
            answer = Vector(self.x - other.x, self.y - other.y)
            # print '__sub__ %s - %s = %s' % (self, other, answer)
            return answer
        # else:
        #     print '__sub__ not vector'

    def __mul__(self, other):
        if isinstance(other, (int, long, float, complex)):
            answer = Vector(self.x*other, self.y*other)
            # print '__mul__ %s * %d = %s' % (self, other, answer)
            return answer
        # else:
        #     print '__mul__ not number'

    def __div__(self, other):
        if isinstance(other, (int, long, float, complex)):
            answer = Vector(self.x/other, self.y/other)
            # print '__div__ %s / %d = %s' % (self, other, answer)
            return answer
        # else:
        #     print '__div__ not number'

    def __neg__(self):
        return Vector(self.x*-1, self.y*-1)

    def __eq__(self, other):
        if (self.x == other.x) and (self.y == other.y):
            return True
        else:
            return False

    def __ne__(self, other):
        if (self.x == other.x) and (self.y == other.y):
            return False
        else:
            return True

    def __str__(self):
        return '(%f, %f)' % (self.x, self.y)

    def magnitude(self):
        return math.sqrt(self.x**2 + self.y**2)

    def unit(self):
        return self/self.magnitude()

    def tuple(self):
        return self.x, self.y


class Dot(object):
    def __init__(self, mass, location, velocity, max_speed):
        self.mass = mass
        self.location = location
        self.velocity = velocity
        self.max_speed = max_speed

    def update(self, time, force):
        self.location = self.location + (self.velocity*time) + ((force*(time**2))/(2*self.mass))
        velocity = self.velocity + ((force*time)/self.mass)
        if self.max_speed and velocity.magnitude() > self.max_speed:
            self.velocity = velocity.unit()*self.max_speed
        else:
            self.velocity = velocity

    def following_force(self, dot, magnitude):
        return ((dot.location - self.location).unit())*magnitude


class Swarm(object):
    def __init__(self, swarm_size, borders, dots_mass, force_magnitude, max_speed):
        self.swarm_size = swarm_size
        self.borders = borders
        self.dots = None
        self.following = None
        self.dots_mass = dots_mass
        self.force_magnitude = force_magnitude
        self.max_speed = max_speed
        self.zero_force = False

    def create_dots(self):
        dots = []
        for i in xrange(self.swarm_size):
            dots += [Dot(self.dots_mass,
                         Vector(random.randint(self.borders[0]), random.randint(self.borders[1])),
                         Vector(0, 0),
                         self.max_speed)]
        self.dots = dots

    def create_following(self):
        following = []
        for i in xrange(self.swarm_size):
            follow = random.randint(self.swarm_size)
            while follow == i:
                follow = random.randint(self.swarm_size)
            following += [follow]
        self.following = following

    def update_dots(self, time):
        for i in xrange(self.swarm_size):
            dot = self.dots[i]
            following = self.dots[self.following[i]]
            if not self.zero_force and (dot.location != following.location):
                dot.update(time, dot.following_force(following, self.force_magnitude))
            else:
                dot.update(time, Vector(0, 0))
        # print self.dots[0].velocity.magnitude()


class Window(QtGui.QMainWindow):
    def __init__(self, width, height, swarm, update_interval):
        super(Window, self).__init__()
        self.resize(width, height)
        self.setStyleSheet("background-color: black;")
        self.swarm = swarm
        self.update_interval = update_interval
        self.update_timer = QTimer()
        self.update_timer.timeout.connect(self.update)
        self.zero_force = False

    def start_swarm(self):
        self.swarm.create_dots()
        self.swarm.create_following()
        self.update_timer.start(1000*self.update_interval)

    def paintEvent(self, event):
        painter = QtGui.QPainter(self)
        pen = QtGui.QPen()
        pen.setWidth(5)
        pen.setCapStyle(Qt.RoundCap)
        pen.setColor(Qt.white)
        painter.setPen(pen)

        self.swarm.update_dots(self.update_interval)
        for dot in self.swarm.dots:
            painter.drawPoint(*dot.location.tuple())

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Space:
            if self.zero_force:
                self.zero_force = False
                self.swarm.zero_force = False
                self.swarm.create_following()
            else:
                self.zero_force = True
                self.swarm.zero_force = True


my_swarm_size = 100
window_width = 1300
window_height = 650
my_dots_mass = 1
my_force_magnitude = 400
my_update_interval = 1.0/24.0
my_max_speed = 100
my_num_following = 10
my_swarm = Swarm(my_swarm_size, (window_width, window_height), my_dots_mass, my_force_magnitude, my_max_speed)

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    window = Window(window_width, window_height, my_swarm, my_update_interval)
    window.show()
    window.start_swarm()
    sys.exit(app.exec_())
